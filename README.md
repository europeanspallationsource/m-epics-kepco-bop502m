# README #

This implements db and protocol for controlling Kepco BOB 50-2M Power Supply by EPICS.
It is based on original EPICS driver delivered by Kepco company. However, few changes had to be made in order to run it.
The original db and protocol files are not included, they are available on kepco web page.
 
Version 1.0.0 
Implements only basic functionality:
1. settings voltage and current.
2. local/remote 
3. output on/off
4. a few common command like identification, reset, read error.
4. implementing of tables which can be later written to a PV of current/voltage/step. Using table user can define ramps, steps and repetition. This is limited to 200 points.

### How do I get set up? ###
This is a standard module use it as any other module developed in EEE
Important is to set the port of stream devices to 5025. In the original documentation the port number is mentioned as 5024.
Setting port to 5024 causes errors due to additional characters/string respond from the device (echo, prompt)
Setting port to 5025 additional characters do not appear in the protocol stream.

### Contribution guidelines ###

### Who do I talk to? ###

author: Tomasz Brys, tomasz.brys@esss.se