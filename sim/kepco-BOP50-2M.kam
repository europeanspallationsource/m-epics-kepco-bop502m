# author: Tomasz Brys 
# email: tomasz.brys@esss.se
# copyright: (C) 2017 European Spallation Source (ESS)
# version: 1.0.0
# date: 2017/JUN/26
# description: File that describes the example simulator in terms of commands/statuses that it receives/sends from/to clients through the TCP/IP protocol.



# The terminator (EOL) of commands/statuses is described in the "TERMINATOR" variable. By default, the terminator is not defined (i.e. empty). If defined, the terminator is inserted at the end of both the commands and statuses received/sent from/to clients before Kameleon starts to process these. The generic form of this variable is:
#
#    TERMINATOR = value
#
# Where value can either be an arbitrary string (e.g. "END") or one of the following pre-defined terminators:
#
#    LF     : the terminator is a line feed (0xA).
#    CR     : the terminator is a carriage return (0xD).
#    LF + CR: the terminator is a line feed (0xA) followed by a carriage return (0xD).
#    CR + LF: the terminator is a carriage return (0xD) followed by a line feed (0xA).
#
# In case there is the need to setup different terminators for commands and statuses, the "TERMINATOR_CMD" and "TERMINATOR_STS" variables can be used respectively (e.g. TERMINATOR_CMD = LF).

TERMINATOR = CR + LF 
#import time


# Data (i.e. commands) received from the client are described in the "COMMANDS" list. The generic form of this list is:
#
#    COMMANDS = [[description_1, command_1, status_1, wait_1], [description_2, command_2, status_2, wait_2], ..., [description_X, command_X, status_X, wait_X]]
#
# Where:
#
#    description: (mandatory) string that describes the command (e.g. "Turn power on").
#    command    : (mandatory) string that represents the command (e.g. "AC1"). Only data (received from the client) that matches exactly the command is selected. Additional matching policies are available:
#                    - if command starts with "***", then any data (received from the client) that ends with command is selected.
#                    - if command ends with "***", then any data (received from the client) that starts with command is selected.
#                    - if command starts and ends with "***", then any data (received from the client) that contains the command is selected.
#    status     : (optional) integer or list that specifies the index(es) of the status(es) (stored in the "STATUSES" list) to send to the client after the command is selected. The first status (stored in "STATUSES" list) is at index 1. If set to 0 or not specified, then no status is sent.
#    wait       : (optional) integer that specifies the time to wait (in milliseconds) before sending the status to the client. If set to 0 or not specified, then the status is immediately sent (i.e. right after the command is received).

COMMANDS = [["List Clear",		 "***LIST:CLE",          "f_ListCle()"       ], #
            ["List Count",		 "***LIST:COUN ***",     "f_ListCoun()"      ], # 
            ["List Count Query",	 "***LIST:COUN?",        1 		     ], # 
            ["List Count Skip",		 "***LIST:COUN:SKIP ***","f_ListCounSkip()"  ], # 
            ["List Count Skip Query",	 "***LIST:COUN:SKIP?",   2 		     ], # 
	    ["List Current",		 "***LIST:CURR ***",     "f_ListCurr()"      ], #
	    ["List Current Query",	 "***LIST:CURR?",        3 		     ], #
            ["List Current Points",	 "***LIST:CURR:POIN?",   4 		     ], #
            ["List Direction",		 "***LIST:DIR ***",      "f_ListDir()"       ], #
            ["List Direction",		 "***LIST:DIR?",         5 		     ], #
            ["List DWeli",		 "***LIST:DWELI ***",    "f_ListDWeli()"     ], #
            ["List DWeli",		 "***LIST:DWELI?",       6 		     ], #
            ["List DWeli Points",	 "***LIST:DWELI:POIN?",  7 		     ], #
            ["List Generation",		 "***LIST:GEN ***",      "f_ListGen()"       ], #
            ["List Generation",		 "***LIST:GEN?",         8 		     ], #
            ["List Query",		 "***LIST:QUER ***",     "f_ListQuer()"      ], # 
            ["List Query Query",	 "***LIST:QUER?",        9 		     ], # 
            ["List Voltage",		 "***LIST:VOLT ***",     "f_ListVolt()"      ], #
            ["List Voltage Query",	 "***LIST:VOLT?",        10 		     ], #
            ["List Voltage Points Query","***LIST:VOLT:POIN?",   11 		     ], #
            ["List Sequence",		 "***LIST:SEQ ***",      "f_ListSeq()"       ], #
            ["List Sequence Query",	 "***LIST:SEQ?",         12	       	     ], #
            ["System Remote",	 	 "SYST:REM ***",         "f_SystemRemote()"  ], #
            ["System Remote Query",	 "SYST:REM?",            13	       	     ], #
            ["Output State",	 	 "OUTPUT ***",           "f_Output()"        ], #
            ["Output State Query",	 "OUTPUT?",              14	       	     ], #
            ["Set Voltage",	 	 "SOUR:VOLT ***",        "f_SetVoltage()"    ], #
            ["Set Current",	  	 "SOUR:CURR ***",        "f_SetCurrent()"    ], #
            ["Get Voltage",	 	 "SOUR:VOLT?",           15                  ], #
            ["Get Current",	 	 "SOUR:CURR?",           16	       	     ], #
            ["Measure Voltage Query",	 "MEAS:VOLT?",           17                  ], #
            ["Measure Current Query",	 "MEAS:CURR?",           18	       	     ], #
            ["Instrument ID",	         "*IDN?",                19	       	     ], #
            ["Operation Mode",	         "FUNC:MODE ***",        "f_FuncMode()"	     ], #
            ["Execution Volt list",	 "VOLT:MODE ***",        "f_VoltMode()"      ], #
            ["Execution Curr list",	 "CURR:MODE ***",        "f_CurrMode()"	     ], #
            ["Operation Mode Query",	 "FUNC:MODE?",           20	       	     ], #
            ["Reset device",	         "*RST",                 "f_Reset()"         ]] #

# Data (i.e. statuses) sent to the client are described in the "STATUSES" list. The generic form of this list is:
#
#    STATUSES = [[description_1, behavior_1, value_1, prefix_1, suffix_1, timeout_1], [description_2, behavior_2, value_2, prefix_2, suffix_2, timeout_2]], ..., [description_X, behavior_X, value_X, prefix_X, suffix_X, timeout_X]]
#
# Where:
#
#    description: (mandatory) string that describes the status (e.g. "Get power status").
#    behavior   : (mandatory) integer that specifies the behavior for generating the status. It can either be:
#                    - FIXED (sends a fixed value to the client)
#                    - ENUM (sends a value - belonging to an enumeration - to the client)
#                    - INCR (sends an incremented value to the client)
#                    - RANDOM (sends a random value to the client)
#                    - CUSTOM (sends a custom value to the client)
#    value      : (mandatory) value to send to the client. Depending on the behavior, it can either be an integer, float, string or list:
#                    - when FIXED, the value is expected to be an integer, float or string. Independently of how many times it is sent to the client, the value remains the same (i.e. does not change).
#                    - when ENUM, the value is expected to be a list. It represents a set of elements (enumeration). After sending an element of the list to the client, the next value to be sent is the next element in the list. When the last element is sent, the next to be sent is the the first element of the list.
#                    - when INCR, the value is expected to be an integer, float or list. If an integer, then the first value to be sent is a 0 and subsequent values to be sent are incremented by value. If a list, then the lower bound, upper bound and increment values are defined by the first, second and third elements of the list, respectively.
#                    - when RANDOM, the value is expected to be an integer or a list. If an integer, then a random number between 0 and value is generated. If a list, then the lower and upper bounds of the random number to generate are defined by the first and second elements of the list, respectively. The generated random number is sent to the client.
#                    - when CUSTOM, the value is expected to be a string. It contains the name of an user-defined Python function to be called by Kameleon. The returned value of this function is sent to the client.
#    prefix     : (optional) string that contains the prefix to insert at the beginning of the value to send to the client. If not specified, then nothing is inserted.
#    suffix     : (optional) string that contains the suffix to insert at the end of the value to send to the client. If not specified, then nothing is inserted.
#    timeout    : (optional) integer that specifies the time-out (in milliseconds) after which the status is sent to the client (i.e. time-based). If set to 0 or not specified, then the status is only sent after receiving a command from the client (i.e. event-based).

STATUSES = [["List Count Query",	CUSTOM, "f_ListCounQuery()"       ], #  1
            ["List Count Skip",		CUSTOM, "f_ListCounSkipQuery()"   ], #  2
            ["List Current",		CUSTOM, "f_ListCurrQuery()"       ], #  3
            ["List Current Points",	CUSTOM, "f_ListCurrPointsQuery()" ], #  4 
            ["List Direction",		CUSTOM, "f_ListDirQuery()"        ], #  5
            ["List DWeli",		CUSTOM, "f_ListDWeliQuery()"      ], #  6
            ["List DWeli Points",	CUSTOM, "f_ListDWeliPointsQuery()"], #  7
            ["List Generation",		CUSTOM, "f_ListGenQuery()"        ], #  8
            ["List Query",		CUSTOM, "f_ListQuerQuer()"        ], #  9
            ["List Voltage",		CUSTOM, "f_ListVoltQuery()"       ], # 10 
            ["List Voltage Points",	CUSTOM, "f_ListVoltPointsQuery()" ], # 11
            ["List Sequence",		CUSTOM, "f_ListSeqQuery()"        ], # 12
	    ["System Remote",	        CUSTOM, "f_SystemRemoteQuery()"   ], # 13
            ["Output State",	        CUSTOM, "f_OutputQuery()"         ], # 14
            ["Get Voltage",	 	CUSTOM, "f_GetVoltageQuery()"     ], # 15
            ["Get Current",	        CUSTOM, "f_GetCurrentQuery()"     ], # 16
            ["Measure Voltage",	        CUSTOM, "f_MeasVoltQuery()"       ], # 17
            ["Measure Current",	        CUSTOM, "f_MeasCurrQuery()"       ], # 18
            ["Instrument ID",	        FIXED,  "KEPCO BOP 50-2M"         ], # 19
            ["Function Mode Query",	CUSTOM, "f_FuncModeQuery()"       ]] # 20


# User-custom Python code

# global variables
g_dir	 = 'UP'
g_gen	 = 0
g_dseq	 = 0
g_count	 = 1
g_listC  = []
g_listV  = []
g_listS  = []
g_dweli  = 0
g_pointC = 0
g_pointV = 0

g_remote = 0
g_output = 0
g_volt   = 0
g_curr   = 0
g_currMode = ''
g_voltMode = ''
g_funcmode = 0; # 0 - volt, 1 - curr

#########################################################
def f_ListCle():
    global g_dir 
    global g_gen 
    global g_dseq 
    global g_count 
    global g_skip 
    global g_pointC
    global g_pointV
    global g_listC
    global g_listV
    g_dir   = 'UP'
    g_gen   = 0
    g_dseq  = 0
    g_count = 1
    g_skip  = 0
    g_pointC = 0
    g_pointV = 0
    g_listC = []
    g_listV = []
    g_listS = []
    return

#########################################################
def f_ListCoun():
    global g_count 
    g_count = int(COMMAND_RECEIVED[10:])      
    return

#########################################################
def f_ListCounQuery():
    return g_count

#########################################################
def f_ListCounSkip():     
    global g_skip 
    g_skip = int(COMMAND_RECEIVED[15:])      
    return

#########################################################
def f_ListCounSkipQuery():     
    return g_skip

#########################################################
def f_ListCurr():      
    global g_listC
    g_listC = COMMAND_RECEIVED[10:].split(',')
    g_listC = [int(i) for i in g_listC]
    print g_listC
    return

#########################################################
def f_ListCurrQuery():      
    return

#########################################################
def f_ListCurrPointsQuery():
   return g_list.length 


#########################################################
def f_ListDir():    
    global g_dir 
    print COMMAND_RECEIVED[9:10]      
    if COMMAND_RECEIVED[9:10] == 'UP':
       g_dir = 'UP'
    elif COMMAND_RECEIVED[9:10] == 'DO':
       g_dir = 'DOWN'     
    return

#########################################################
def f_ListDirQuery():    
    return g_dir

#########################################################
def f_ListDWeli():        
    global g_listS
    g_listS = COMMAND_RECEIVED[10:].split(',')
    g_listS = [float(i) for i in g_listS]
    print g_listS
    return

#########################################################
def f_ListDWeliPoints():  
    return


#########################################################
def f_ListGen():   
    return


#########################################################
def f_ListQuery():        
    return


#########################################################
def f_ListVolt():      
    global g_listV
    g_listV = COMMAND_RECEIVED[10:].split(',')
    g_listV = [float(i) for i in g_listV]
    print g_listV
    return


#########################################################
def f_ListVoltQuery():      
    return

#########################################################
def f_ListVoltPointsQuery():
    return g_list.length 


#########################################################
def f_ListSeq():      
    return

#########################################################
def f_ListSeqQuery():      
    return

  
#########################################################
def f_SystemRemote():
    global g_remote
    #print 'COMMAND_RECEIVED[6:]', COMMAND_RECEIVED[8:]
    g_remote = int(COMMAND_RECEIVED[8:])      
    return

#########################################################
def f_Output():      
    global g_output
    #print 'COMMAND_RECEIVED[6:]', COMMAND_RECEIVED[6]
    g_output = int(COMMAND_RECEIVED[6:])      
    return

#########################################################
def f_SetVoltage():      
    global g_volt
    #print 'COMMAND_RECEIVED[9:]', COMMAND_RECEIVED[10:]
    g_volt = float(COMMAND_RECEIVED[10:])  
        
    return

#########################################################
def f_SetCurrent():      
    global g_curr
    #print 'COMMAND_RECEIVED[9:]', COMMAND_RECEIVED[10:]
    g_curr = float(COMMAND_RECEIVED[10:])      
    return

#########################################################
def f_SystemRemoteQuery():
    global g_remote
    return g_remote

#########################################################
def f_OutputQuery():      
    global g_output
    return g_output

#########################################################
def f_GetVoltageQuery():      
    global g_volt
    return g_volt

#########################################################
def f_GetCurrentQuery():      
    global g_curr
    return g_curr

#########################################################
def f_MeasVoltQuery():
    global g_volt
    return g_volt

#########################################################
def f_MeasCurrQuery():
    global g_curr
    return g_curr

#########################################################
def f_FuncModeQuery():
    global g_funcmode
    return g_funcmode

#########################################################
def f_FuncMode():
    global g_funcmode
    if COMMAND_RECEIVED[10:14] == 'VOLT':
       g_funcmode = 0
    elif COMMAND_RECEIVED[10:14] == 'CURR':
       g_funcmode = 1
      
    return 

#########################################################
def f_Reset():
    return
#########################################################
def f_VoltMode():
    global g_voltMode
    g_voltMode = COMMAND_RECEIVED[10:]

#########################################################
def f_CurrMode():
    global g_currMode
    g_currMode = COMMAND_RECEIVED[10:]
