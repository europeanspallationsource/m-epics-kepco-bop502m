# pvs[0]:  loc://do_write_the_column(0), triggering the script execution
# pvs[1]:  ListVolt, array PV to which you want to write, NOT triggering the script
# pvs[2]:  ListCurr, array PV to which you want to write, NOT triggering the script
# pvs[3]:  ListDwel, array PV to which you want to write, NOT triggering the script
# pvs[4]:  Curr PV, set limit in voltage mode
# pvs[5]:  Volt PV, set limit in current mode
# pvs[6]:  RST PV, reset the device 
# pvs[7]:  ListCLE PV, clear all the list in the device
# pvs[8]:  ListCount PV, number of loops
# pvs[9]:  ListDIR PV, direction of execution 
# pvs[10]: Output PV, output on/off
# pvs[11]: OutputMode PV, mode voltage/current
# pvs[12]: VoltMode PV,
# pvs[13]: CurrMode PV,

from org.csstudio.opibuilder.scriptUtil import ConsoleUtil
from org.csstudio.opibuilder.scriptUtil import PVUtil
from array import array



if PVUtil.getLong(pvs[0]) > 0:
    # Get the table content
    table = widget.getTable()
    content = table.getContent()
    spinner = display.getWidget('NLoops')
    sp = spinner.getValue()

    
    ConsoleUtil.writeString("curr:: pvs[4]");
    
    # print PVUtil.getString(pvs[14])
    # PVUtil.print Hello
    # print content
    # Get table's column 1,2 (counting from 0) into val,dwel
    val =  [float(r[1]) for r in content]
    dwel = [float(r[2]) for r in content]
    
    pvs[6].setValue(1)                       # Reset
    pvs[4].setValue(PVUtil.getDouble(pvs[4]))                 # Set current limit 
    pvs[7].setValue(1)                       # clear all lists
    
    if PVUtil.getString(pvs[14]) == 'UP':
       pvs[1].setValue(array('d', val))      # write voltage list to device       
       pvs[3].setValue(array('d', dwel))     # set steps
    elif PVUtil.getString(pvs[14]) == 'DOWN':
       pvs[2].setValue(array('d', val))      # write current list to device
       pvs[3].setValue(array('d', dwel))     # set steps
    
    
    pvs[8].setValue(sp)                      # set count
    pvs[9].setValue('UP')                    # set direction 
    pvs[10].setValue(1)                      # set output on
    pvs[12].setValue('LIST')                 # set output mode list
   