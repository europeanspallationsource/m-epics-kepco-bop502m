from org.csstudio.opibuilder.scriptUtil import PVUtil
from array import array
from java.lang import String


value = display.getWidget("NValue")
dweli = display.getWidget("NDweli")
tab   = display.getWidget("Table")

r = tab.getTable().getRowCount()

# number of rows is limited in this script to 200. The device support 1002 points but writing and reading is more complicated
# and must be spread in few commands, thus only 200 points was chosen (can be written in one command)

if r < 200:             
   a = str(tab.getTable().getRowCount())
   b = str(value.getValue())
   c = str(dweli.getValue())

   tab.getTable().setCellText(r, 0, a)
   tab.getTable().setCellText(r, 1, b)
   tab.getTable().setCellText(r, 2, c)
