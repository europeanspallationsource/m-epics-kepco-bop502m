require kepco-bop502m,1.0.0
 
epicsEnvSet(EPICS_CA_ADDR_LIST,10.0.2.15)

#Specify the TCP endpoint and give your 'bus' an arbitrary name eg. "kepco-stream".
#drvAsynIPPortConfigure("kepco-stream", "10.4.0.251:5025")
drvAsynIPPortConfigure("kepco-stream", "10.4.3.113:5025")
 
 asynSetTraceMask("kepco-stream", -1, 0x9)
 asynSetTraceIOMask("kepco-stream", -1, 0x2)


## Load record instances
## dbLoadRecords(kepco-BOP502M.db,"P=KPwrBOP,PORT=kepco-stream,R=:,A=-1")
dbLoadRecords(kepco-BOP502M.db,"P=BOP,PORT=kepco-stream,R=:,A=-1")

